import { Injectable } from '@angular/core';
import { IPL, IPLTeam, Match, MatchApp, MatchBatman } from '../classes/app.interface';

@Injectable({
  providedIn: 'root',
})
export class HelperService {
	constructor() { }

	get(data: IPL): Match {
		const team1 = data.matchInfo.teams[0].team.abbreviation;
		const team2 = data.matchInfo.teams[1].team.abbreviation;

		const players = data.matchInfo.teams[0].players.concat(data.matchInfo.teams[1].players);
		let score1 = '';
		let score2 = '';
		let overs1 = '';
		let overs2 = '';
		let crr = '';
		let status = '';
		let partnerShip = '';
		let bat1: MatchBatman = {};
		let bat2: MatchBatman = {};
		let bowl1: MatchBatman = {};
		let bowl2: MatchBatman = {};
		let recentOvs;
		let partnership;
		let showTeam = false;
		let startTime = 0;

		if (data.matchInfo.matchDateMs > new Date().getTime()) {
			startTime = (data.matchInfo.matchDateMs - new Date().getTime()) / 1000;
			showTeam = true;
		}

		if (data.innings[0]) {
			score1 = data.innings[0].scorecard.runs + '/' + data.innings[0].scorecard.wkts;
			overs1 = data.innings[0].overProgress;
			crr = data.innings[0].runRate;
		}
		if (data.innings[1]) {
			score2 = data.innings[1].scorecard.runs + '/' + data.innings[1].scorecard.wkts;
			overs2 = data.innings[1].overProgress;
			crr = data.innings[1].runRate;
		}

		let currentInn = data.innings[data.currentState.currentInningsIndex];

		if (currentInn && currentInn.scorecard) {
			bat1 = currentInn.scorecard.battingStats.find(p => p.playerId == data.currentState.facingBatsman) || {};
			if (bat1) bat1.name = players.find(p => p.id == bat1.playerId)?.fullName;

			bat2 = currentInn.scorecard.battingStats.find(p => p.playerId == data.currentState.nonFacingBatsman) || {};
			if (bat2) bat2.name = players.find(p => p.id == bat2.playerId)?.fullName;

			bowl1 = currentInn.scorecard.bowlingStats.find(p => p.playerId == data.currentState.currentBowler) || {};
			if (bowl1) bowl1.name = players.find(p => p.id == bowl1.playerId)?.fullName;

			bowl2 = currentInn.scorecard.bowlingStats.find(p => p.playerId == data.currentState.previousBowler) || {};
			if (bowl2) {
				bowl2.name = players.find(p => p.id == bowl2.playerId)?.fullName;
				if (!bowl1.name) bowl1 = bowl2;
			}
		}

		if (data.currentState.recentOvers)
			recentOvs = data.currentState.recentOvers.slice(Math.max(data.currentState.recentOvers.length - 3, 0))

		status = data.matchInfo.additionalInfo['toss.elected'] || data.matchInfo.additionalInfo['match.summary'] ;

		if (data.currentState.currentInningsIndex && data.currentState.currentInningsIndex == 1) {
			if (data.matchInfo.inningsSummary && data.matchInfo.inningsSummary.type == 'C') {
				const s = data.matchInfo.inningsSummary.values;
				status = `${team2} require ${s[0]} runs with ${s[1]} balls remaining`
			}
		}

		if (data.matchInfo.matchStatus) status = data.matchInfo.matchStatus.text;

		if (data.currentState.partnership)
			partnerShip = `${data.currentState.partnership.runs} (${data.currentState.partnership.balls})`

		return {
			team1: team1,
			team2: team2,
			score1: score1,
			score2: score2,
			overs1: overs1,
			overs2: overs2,
			crr: crr,
			bat1: bat1,
			bat2: bat2,
			bowl1: bowl1,
			bowl2: bowl2,
			recentOvs: recentOvs,
			status: status,
			rrr: data.currentState.requiredRunRate,
			partnerShip: partnerShip,
			teams: data.matchInfo.teams,
		}
	}

	exchangeTo(response: any): MatchApp {
		let ro = response['ro'];
		let lb = { p: '',	s: '' };
		let lw = { p: '', s: '' };

		if (response['h1'] && response['h1']['a'] == "Last Bowler") {
			lb = this.lastBowlerWicket(response, 'h1');
			lw = this.lastBowlerWicket(response, 'h2');
		} else {
			lb = this.lastBowlerWicket(response, 'h2');
			lw = this.lastBowlerWicket(response, 'h1');
		}
		let t1 = response['team1'];
		let t2 = response['team2'];


		let score1A = t1.replace(/ *\([^)]*\) */g, '').replace(/[^0-9]/g, '');
		let score2A = t2.replace(/ *\([^)]*\) */g, '').replace(/[^0-9]/g, '');
		response['team1'] = t1.replace(/[^a-zA-Z]+/g, '');
		response['team2'] = t2.replace(/[^a-zA-Z]+/g, '');

		let team1 = response['team1'] || '';
		let team2 = response['team2'] || '';

		return {
			team1Short: team1,
			team2Short: team2,
			team1: this.getTeamName(team1),
			team2: this.getTeamName(team2),
			score1A: response['score1A'] || '',
			score1B: response['score1B'] || '',
			score2A: response['score2A'] || '',
			score2B: response['score2B'] || '',
			over1A: response['over1A'] || '',
			over1B: response['over1B'] || '',
			over2A: response['over2A'] || '',
			over2B: response['over2B'] || '',
			crr: response['crr'] || '-',
			rrr: response['rrr'] || '',
			ovLeft: response['ovLeft'] || '',
			toss: response['toss'] || '-',
			b1: {
				i: this.getPlayerImage(response['p1']['i']),
				ib: response['p1']['ib'] || false,
				p: response['p1']['p'] || '-',
				r: response['p1']['r'] || '-',
				b: response['p1']['b'] || '-',
				sr: response['p1']['sr'] || '-',
				f: response['p1']['f'] || '-',
				s: response['p1']['s'] || '-',
			},
			b2: {
				i: this.getPlayerImage(response['p2']['i']),
				ib: response['p2']['ib'] || false,
				p: response['p2']['p'] || '-',
				r: response['p2']['r'] || '-',
				b: response['p2']['b'] || '-',
				sr: response['p2']['sr'] || '-',
				f: response['p2']['f'] || '-',
				s: response['p2']['s'] || '-',
			},
			b3: {
				i: this.getPlayerImage(response['p3']['i']),
				p: response['p3']['p'] || '-',
				o: response['p3']['o'] || '-',
				r: response['p3']['r'] || '-',
				w: response['p3']['w'] || '-',
				m: response['p3']['m'] || '0',
				e: response['p3']['e'] || '-',
			},
			lb: lb,
			lw: lw,
			ro: this.reformatRecent(ro),
			lo: response['lo'].reverse(),
			status: response['status'],
			lastStatus: response['lastStatus'],
			ps: response['ps'] || '',
		}
	}

	cricbuzzTo(response: any): MatchApp {
		let ro = response['recentOvs'];
		if (ro) ro = ro.split(' ').reverse();
		let lb = this.lastBowler(response);
		let lw = this.lastWicket(response);
		return {
			team1Short: response['team1Full'] || '',
			team2Short: response['team2Full'] || '',
			team1: response['team1Full'] || '',
			team2: response['team2Full'] || '',
			crr: response['crr'] || '-',
			rrr: response['rrr'] || '',
			toss: response['toss'] || '-',
			b1: {
				ib: true,
				i: response['bat1']['name'] || '-',
				p: response['bat1']['name'] || '-',
				r: response['bat1']['r'] || '-',
				b: response['bat1']['b'] || '-',
				sr: response['bat1']['sr'] || '-',
				f: response['bat1']['4s'] || '-',
				s: response['bat1']['6s'] || '-',
			},
			b2: {
				ib: false,
				i: response['bat1']['name'] || '-',
				p: response['bat2']['name'] || '-',
				r: response['bat2']['r'] || '-',
				b: response['bat2']['b'] || '-',
				sr: response['bat2']['sr'] || '-',
				f: response['bat2']['4s'] || '-',
				s: response['bat2']['6s'] || '-',
			},
			b3: {
				i: response['bat1']['name'] || '-',
				p: response['bowl1']['name'] || '-',
				o: response['bowl1']['ov'] || '-',
				r: response['bowl1']['r'] || '-',
				w: response['bowl1']['w'] || '-',
				m: response['bowl1']['maid'] || '-',
				e: response['bowl1']['e'] || '-',
			},
			lb: lb,
			lw: lw,
			ro: this.reformatRecent(ro),
			status: response['status'],
			lastStatus: response['lastStatus'] || '',
			ps: response['partnerShip'] || '',
		}
	}

	getRecordKey(response: any) {
		let key = '';
		if (response['point-table']) key = 'point-table';
		if (response['most-runs']) key = 'most-runs';
		if (response['most-runs-all']) key = 'most-runs-all';
		if (response['most-wkts']) key = 'most-wkts';
		if (response['most-wkts-all']) key = 'most-wkts-all';
		if (response['most-sixes']) key = 'most-sixes';
		if (response['most-sixes-all']) key = 'most-sixes-all';
		if (response['team1']) key = 'team1';
		if (response['team2']) key = 'team2';
		if (response['team1-bat']) key = 'team1-bat';
		if (response['team2-bat']) key = 'team2-bat';
		if (response['team1-bowl']) key = 'team1-bowl';
		if (response['team2-bowl']) key = 'team2-bowl';
		if (response['players']) key = 'players';
		if (response['weather']) key = 'weather';
		if (response['head-to-head']) key = 'head-to-head';
		if (response['series-info']) key = 'series-info';
		if (response['previous-encounter']) key = 'previous-encounter';
		if (response['group1-table']) key = 'group1-table';
		if (response['group2-table']) key = 'group2-table';
		if (response['umpires']) key = 'umpires';
		return key;
	}

	getFlashKey(response: any) {
		let key = '';
		if (response['inn1A']) key = 'inn1A';
		if (response['inn1B']) key = 'inn1B';
		if (response['inn2A']) key = 'inn2A';
		if (response['inn2B']) key = 'inn2B';
		if (response['team1']) key = 'team1';
		if (response['team2']) key = 'team2';
		if (response['players']) key = 'players';
		return key;
	}

	lastBowlerWicket(response: any, key: string) {
		return {
			p: response[key] ? response[key]['b'] : '',
			s: response[key] ? response[key]['c'] : ''
		}
	}

	lastBowler(response: any) {
		if (response['bowl2']['name']) {
			return {
				p: response['bowl2']['name'],
				s: `${response['bowl2']['w']}-${response['bowl2']['r']} (${response['bowl2']['ov']})`
			}
		}
		return {
			p: '',
			s: ''
		}
	}

	lastWicket(response: any) {
		return {
			p: response['lastWkt'] || '',
			s: ''
		}
	}

	reformatRecent(recents: string[]) {
		let ovs: string[] = [];
		recents.forEach(o => {
			ovs.push(o.trim());
		});
		ovs = ovs.filter(o => o != '');
		return ovs;
	}

	reformatTossTeam(team: string) {
		return team.replace('Knight Riders', '').replace('Super Kings', '');
	}

	getTeamName(name: string): string {
		name = name.trim();
		let fullName: any = {
			'RSA': 'SOUTH AFRICA',
			'AUS': 'AUSTRALIA',
			'ENG': 'ENGLAND',
			'AFG': 'AFGHANISTAN',
			'NAM': 'NAMIBIA',
			'IND': 'INDIA',
			'NZ': 'NEW ZEALAND',
			'PAK': 'PAKISTAN',
			'WI': 'WEST INDIES',
			'SL': 'SRI LANKA',
		};
		return fullName[name] || name;
	}

	getShortName(name: string): string {
		let fullName: any = {
			'AUSTRALIA': 'AUS',
			'ENGLAND': 'ENG'
		};
		return fullName[name] || name;
	}

	getPlayerImage(id: string) {
		let players: { [unit: string]: number } = {
			//AUSTRALIA
			'N9': 935, //Ashton Agar
			'95': 857, //JOSH HAZLEWOOD
			'62': 221, //MITCHELL MARSH
			'90': 798, //KANE RICHARDSON
			'8G': 490, //MITCHELL STARC
			'LW': 3780, //MITCHELL SWEPSON
			'4O': 170, //DAVID WARNER
			'8F': 488, //PAT CUMMINS
			'20O': 4088, //JOSH INGLIS
			'7Q': 282, //GLENN MAXWELL
			'7F': 271, //STEVE SMITH
			'9J': 964, //MARCUS STOINIS
			'10L': 290, //MATTHEW WADE
			'9I': 958, //ADAM ZAMPA
			'4L': 167, //AARON FINCH
			//SOUTH AFRICA
			'92': 834, //QUINTON DE KOCK
			'JF': 1915, //REEZA HENDRICKS
			'HC': 1894, //KESHAV MAHARAJ
			'54': 187, //DAVID MILLER
			'DD': 3746, //LUNGI NGIDI
			'NG': 837, //DWAINE PRETORIUS
			'D6': 3309, //TABRAIZ SHAMSI
			'11X': 13778, //BJORN FORTUIN
			'K0': 3869, //HEINRICH KLAASEN
			'EY': 1667, //AIDEN MARKRAM
			'OO': 3070, //WIAAN MULDER
			'YI': 5433, //ANRICH NORTJE
			'B4': 1664, //KAGISO RABADA
			'JS': 1277, //RASSIE VAN DER DUSSEN
			'F9': 833, //TEMBA BAVUMA
			//'2OR': 17068, //Marco Jansen
			//ENGLAND
			'B8': 1735, //MOEEN ALI
			'CD': 2756, //SAM BILLINGS
			'KW': 3646, //TOM CURRAN
			'DB': 3644, //LIAM LIVINGSTONE
			'D7': 3319, //TYMAL MILLS
			'BS': 1906, //JASON ROY
			'9K': 967, //CHRIS WOAKES
			'8I': 506, //JONNY BAIRSTOW
			'8J': 509, //JOS BUTTLER
			'AO': 1299, //CHRIS JORDAN
			'G0': 3318, //DAWID MALAN
			'H4': 796, //ADIL RASHID
			'MV': 2758, //DAVID WILLEY
			'CC': 2749, //MARK WOOD
			'5E': 197, //EOIN MORGAN
			//WEST INDIES
			'B6': 1703, //NICHOLAS POORAN
			'FE': 3321, //ROSTON CHASE
			'6H': 236, //CHRIS GAYLE
			'J6': 872, //EVIN LEWIS
			'8Q': 594, //RAVI RAMPAUL
			'8R': 595, //LENDL SIMMONS
			'1CS': 13722, //HAYDEN WALSH
			'N': 25, //DWAYNE BRAVO
			'JR': 1490, //ANDRE FLETCHER
			'B7': 1705, //SHIMRON HETMYER
			'NR': 3100, //OBED MCCOY
			'4U': 177, //ANDRE RUSSELL
			'EG': 5146, //OSHANE THOMAS
			'21J': 20593, //AKEAL HOSEIN
			'5R': 210, //KIERON POLLARD
			'214': 1902, //Kyle Mayers
			'1CQ': 1708, //Brandon King
			'D8': 3333, //Rovman Powell
			'L0': 1707, //Fabian Allen
			'1CR': 5036, //Romario Shepherd
			'M0': 3103, //Odean Smith
			'LB': 1309, //Sheldon Cottrell
			//INDIA
			'6': 8, //RAVICHANDRAN ASHWIN
			'DE': 3763, //RAHUL CHAHAR
			'7': 9, //RAVINDRA JADEJA
			'2K': 94, //MOHAMMAD SHAMI
			'CL': 2972, //RISHABH PANT
			'2X': 107, //ROHIT SHARMA
			'EK': 5432, //VARUN CHAKARAVARTHY
			'AJ': 1124, //JASPRIT BUMRAH
			'CM': 2975, //ISHAN KISHAN
			'36': 116, //BHUVNESHWAR KUMAR
			'C3': 2740, //HARDIK PANDYA
			'AK': 1125, //KL RAHUL
			'BG': 1745, //SHARDUL THAKUR
			'2Y': 108, //SURYAKUMAR YADAV
			'4I': 164, //VIRAT KOHLI
			'B1': 1563, //Shreyas Iyer
			'A9': 1113, //Axar Patel
			'3U': 140, //Deepak Chahar
			'E1': 3840, //Mohammed Siraj
			'ES': 5443, //ruturaj-gaikwad
			'31': 111, //yuzvendra-chahal
			'AZ': 1561, //avesh-khan
			'4B': 157, //harshal-patel
			'E': 16, //Wriddhiman Saha
			'O5': 3761, //Shubman Gill
			'4C': 158, //Mayank Agarwal
			'4A': 156, //Cheteshwar Pujara
			'3P': 135, //Ajinkya Rahane
			'10': 38, //Ishant Sharma
			'1L': 59, //Umesh Yadav
			'BX': 2734, //ks bharat
			'EF': 5105, //prasidh krishna
			'BB': 1740, //jayant-yadav
			'5S8': 22245, //Venkatesh Iyer
			'16E': 19351, //Ravi Bishnoi
			//NEW ZEALAND
			'IO': 2897, //TODD ASTLE
			'JV': 1358, //MARK CHAPMAN
			'DC': 3729, //Lockie FERGUSON
			'13F': 1616, //KYLE JAMIESON
			'12X': 971, //JIMMY NEESHAM
			'BR': 1903, //MITCHELL SANTNER
			'AP': 1304, //ISH SODHI
			'9M': 969, //TRENT BOULT
			'4W1': 4650, //DEVON CONWAY
			'8A': 431, //MARTIN GUPTILL
			'MP': 3027, //GLENN PHILLIPS
			'JX': 1619, //TIM SEIFERT
			'7Z': 307, //TIM SOUTHEE
			'8C': 440, //KANE WILLIAMSON
			'8B': 434, //ADAM MILNE
			'4W3': 3028, //Rachin Ravindra
			'F0': 1140, //Tom Latham
			'1VT': 4596, //Will Young
			'67': 226, //Ross Taylor
			'EW': 2890, //Henry Nicholls
			'KN': 3743, //Tom Blundell
			'IS': 4599, //Ajaz Patel
			'IP': 4157, //William Somerville
			//AFGHANISTAN
			'KV': 1498, //SHARAFUDDIN ASHRAF
			'HL': 611, //ASGHAR AFGHAN
			'102': 614, //HAMID HASSAN
			'JE': 5134, //HAZRATULLAH ZAZAI
			'I6': 619, //MOHAMMAD SHAHZAD
			'JH': 620, //NAJIBULLAH ZADRAN
			'10U': 4681, //RAHMANULLAH GURBAZ
			'K3': 1508, //USMAN GHANI
			'ME': 613, //GULBADIN NAIB
			'4EK': 2884, //HASHMATULLAH SHAHIDI
			'N1': 2901, //KARIM JANAT
			'11F': 4572, //MUJEEB UR RAHMAN
			'1CE': 3122, //NAVEEN-UL-HAQ
			'CE': 2885, //RASHID KHAN
			'MA': 2883, //FAREED AHMAD
			'8S': 618, //MOHAMMAD NABI
			//NAMIBIA
			'2WV': 1220, //STEPHAN BAARD
			'68K': 2842, //MICHAU DU PREEZ
			'6WG': 1601, //ZANE GREEN
			'2WZ': 1225, //BERNARD SCHOLTZ
			'2X0': 1227, //JJ SMIT
			'67W': 3002, //MICHAEL VAN LINGEN
			'29J': 1231, //CRAIG WILLIAMS
			'67V': 17105, //KARL BIRKENSTOCK
			'68G': 2854, //JAN FRYLINCK
			'689': 4554, //NICOL LOFTIE-EATON
			'682': 4558, //BEN SHIKONGO
			'68B': 21107, //RUBEN TRUMPELMANN
			'97': 869, //DAVID WIESE
			'6WH': 17106, //PIKKY YA FRANCE
			'29O': 1223, //GERHARD ERASMUS
			//PAKISTAN
			'F2': 2759, //BABAR AZAM
			'JW': 1265, //ASIF ALI
			'17L': 19206, //HAIDER ALI
			'HQ': 3325, //HASAN ALI
			'JL': 441, //MOHAMMAD HAFEEZ
			'NA': 1201, //MOHAMMAD RIZWAN
			'GE': 3046, //SHADAB KHAN
			'IZ': 4530, //SHAHEEN AFRIDI
			'H6': 3801, //FAKHAR ZAMAN
			'13N': 5404, //HARIS RAUF
			'N6': 2760, //IMAD WASIM
			'LU': 3195, //MOHAMMAD NAWAZ
			'18O': 19406, //MOHAMMAD WASIM
			'FC': 846, //SARFARAZ AHMED
			'J9': 314, //SHOAIB MALIK
		}
		//let image = `https://playerimages.platform.bcci.tv/t20i/260x350/Photo-Missing.png`;
		let image = 'https://resources.pulse.icc-cricket.com/players/130x150/Photo-Missing.png'
		if (players[id]) {
			//image = `https://playerimages.platform.bcci.tv/test/520x700/${players[id]}.png`;
			//image = `https://playerimages.platform.bcci.tv/t20i/260x350/${players[id]}.png`;
			image = `https://resources.pulse.icc-cricket.com/players/210/${players[id]}.png`;
		}

		if (players[id] == 2975 || players[id] == 108 || players[id] == 8540
			|| players[id] == 157 || players[id] == 140 || players[id] == 116
			|| players[id] == 19351 || players[id] == 1902 || players[id] == 100020
			|| players[id] == 100055 || players[id] == 100018 || players[id] == 3103
			|| players[id] == 20593
		)
			image = 'https://resources.pulse.icc-cricket.com/players/130x150/Photo-Missing.png'
		return image;
	}
}