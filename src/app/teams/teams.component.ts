import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { IPLTeam, Match, MatchTeam } from 'src/app/classes/app.interface';
import { HelperService } from 'src/app/services/helper.service';

interface Record {
	[key: string]: boolean;
}

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {
	matchTeam: MatchTeam = {};
	team1?: IPLTeam;
	team2?: IPLTeam;
	data: Record = {};
	allPlayers: any = {};

	constructor(
		private fireStore: AngularFirestore,
		private helperService: HelperService
	) {
		this.fireStore.collection("teams").doc("ipl").valueChanges().subscribe((response) => {
			this.matchTeam  = response as MatchTeam;
			this.team1 = this.matchTeam.team1;
			this.team2 = this.matchTeam.team2;
		});
		this.fireStore.collection("flash").doc("scorecard").valueChanges().subscribe((response) => {
			this.data = response as Record;
		});

		this.fireStore.collection("players").valueChanges().subscribe((response) => {
			response.forEach((k: any) => {
				this.allPlayers[k['id']] = k;
			});
		});

	}

  ngOnInit(): void {
	}

	hideAll() {
		Object.keys(this.data).forEach((i) => this.data[i] = false);
		this.fireStore.collection("flash").doc("scorecard").set(this.data);
	}

	toggle(playerId: number, key: string) {
		Object.keys(this.data).forEach((i) => this.data[i] = false);
		this.fireStore.collection("flash").doc("scorecard").set(this.data);

		this.fireStore.collection("scorecard").doc("players").set({ html: this.allPlayers[playerId][key] });

		this.data['players'] = true;
		this.fireStore.collection("flash").doc("scorecard").set(this.data);
	}
}
