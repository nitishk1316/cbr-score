import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CricEdgeRoutingModule } from './cric-edge-exchange-routing.module';
import { CricEdgeExchangeComponent } from './cric-edge-exchange.component';
import { SharedComponentModule } from '../components/shared-component.module';

@NgModule({
  declarations: [CricEdgeExchangeComponent],
  imports: [
    CommonModule,
		SharedComponentModule,
	  CricEdgeRoutingModule
  ],
  exports: [CricEdgeExchangeComponent]
})
export class CricEdgeExchangeModule { }
