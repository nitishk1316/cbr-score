import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CricEdgeExchangeComponent } from './cric-edge-exchange.component';

const routes: Routes = [
  {
    path: '',
    component: CricEdgeExchangeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CricEdgeRoutingModule { }
