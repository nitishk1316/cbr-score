import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CricketFirstRoutingModule } from './cricket-first-routing.module';
import { CricketFirstComponent } from './cricket-first.component';
import { SharedComponentModule } from '../components/shared-component.module';


@NgModule({
  declarations: [
		CricketFirstComponent
	],
  imports: [
    CommonModule,
		SharedComponentModule,
    CricketFirstRoutingModule
  ],
  exports: [CricketFirstComponent]
})
export class CricketFirstExchangeModule { }
