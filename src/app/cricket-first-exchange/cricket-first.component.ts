import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';
import { MatchApp } from '../classes/app.interface';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-cricket-first',
  templateUrl: './cricket-first.component.html',
  styleUrls: ['./cricket-first.component.scss']
})
export class CricketFirstComponent implements OnInit {
	match!: MatchApp;
	data = '';
	toss = '';
	special = '';
	isAds = false;

	constructor(
		private fireStore: AngularFirestore,
		private helperService: HelperService,
		private route: ActivatedRoute
	) {
		this.route.queryParams.subscribe(params => {
			if (params['ads']) this.isAds = true;
		});

		this.fireStore.collection("exchange").doc("live").valueChanges().subscribe((response: any) => {
			setTimeout(() => {
				this.match = 	this.helperService.exchangeTo(response);
			}, 5);
		});

		this.fireStore.collection("flash").doc("scorecard").valueChanges().subscribe((response: any) => {
			this.data = '';
			const key = this.helperService.getFlashKey(response);
			if (key) {
				this.fireStore.collection("scorecard").doc(key).ref.get().then((response: any) => {
					this.data = response.data()['html'];
				});
			}
		});

		this.fireStore.collection("records").doc("toss").valueChanges().subscribe((response: any) => {
			this.toss = this.helperService.reformatTossTeam(response['team']);
		});

		this.fireStore.collection("records").doc("content").valueChanges().subscribe((response: any) => {
			this.special = response['html'];
		});
	}

  ngOnInit(): void {
	}
}
