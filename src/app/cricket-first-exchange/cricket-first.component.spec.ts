import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CricketFirstComponent } from './cricket-first.component';

describe('CricketFirstComponent', () => {
  let component: CricketFirstComponent;
  let fixture: ComponentFixture<CricketFirstComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CricketFirstComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CricketFirstComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
