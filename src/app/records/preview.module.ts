import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RecordRoutingModule } from './preview-routing.module';
import { RecordComponent } from './preview.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [RecordComponent],
  imports: [
    CommonModule,
		RecordRoutingModule,
		FormsModule,
	],
  exports: [RecordComponent]
})
export class RecordModule { }
