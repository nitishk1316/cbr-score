import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { ActivatedRoute } from '@angular/router';

interface Record {
	[key: string]: boolean;
}

@Component({
  selector: 'app-record',
  templateUrl: './preview.component.html',
  styleUrls: ['./preview.component.scss']
})
export class RecordComponent implements OnInit {
	data: Record = {};
	htmlContent = '';
	constructor(
		private fireStore: AngularFirestore,
		private route: ActivatedRoute
	) {
		this.fireStore.collection("flash").doc("scorecard").valueChanges().subscribe((response) => {
			this.data = response as Record;
		});
	}

	ngOnInit() {

	}

	toggle(key: string, value: boolean) {
		Object.keys(this.data).forEach((i) => this.data[i] = false);
		this.data[key] = value;
		this.fireStore.collection("flash").doc("scorecard").set(this.data);
	}

	add() {
		if (this.htmlContent)
			this.fireStore.collection("records").doc("content").set({ html: this.htmlContent });
	}

	remove() {
		this.htmlContent = '';
		this.fireStore.collection("records").doc("content").set({ html: '' });
	}
}
