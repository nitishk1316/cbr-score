import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CricBlastRadioComponent } from './cric-blast-radio.component';

describe('CricBlastRadioComponent', () => {
  let component: CricBlastRadioComponent;
  let fixture: ComponentFixture<CricBlastRadioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CricBlastRadioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CricBlastRadioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
