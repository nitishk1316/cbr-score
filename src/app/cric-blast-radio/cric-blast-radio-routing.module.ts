import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CricBlastRadioComponent } from './cric-blast-radio.component';

const routes: Routes = [
  {
    path: '',
    component: CricBlastRadioComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CricBlastRadioRoutingModule { }
