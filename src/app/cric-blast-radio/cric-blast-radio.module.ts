import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CricBlastRadioRoutingModule } from './cric-blast-radio-routing.module';
import { CricBlastRadioComponent } from './cric-blast-radio.component';

@NgModule({
  declarations: [CricBlastRadioComponent],
  imports: [
    CommonModule,
    CricBlastRadioRoutingModule
  ],
  exports: [CricBlastRadioComponent]
})
export class CricBlastRadioModule { }
