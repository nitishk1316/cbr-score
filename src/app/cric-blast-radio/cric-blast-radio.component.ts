import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { IPL, Match } from '../classes/app.interface';
import { HelperService } from '../services/helper.service';

@Component({
  selector: 'app-cric-blast-radio',
  templateUrl: './cric-blast-radio.component.html',
  styleUrls: ['./cric-blast-radio.component.scss']
})
export class CricBlastRadioComponent implements OnInit {
	match: Match = {};
	data = '';
	constructor(
		private fireStore: AngularFirestore,
		private helperService: HelperService,
		private http: HttpClient
	) { 
		this.fireStore.collection("scores").doc("ipl").valueChanges().subscribe((response) => {
			this.match = response as Match;
		});

		this.fireStore.collection("records").doc("facts").valueChanges().subscribe((response: any) => {
			this.data = '';
			if (response['point-table']) this.getData('point-table');
			if (response['most-runs']) this.getData('most-runs');
			if (response['most-runs-all']) this.getData('most-runs-all');
			if (response['most-wkts']) this.getData('most-wkts');
			if (response['most-wkts-all']) this.getData('most-wkts-all');
			if (response['most-sixes']) this.getData('most-sixes');
			if (response['most-sixes-all']) this.getData('most-sixes-all');
			if (response['team1']) this.getData('team1');
			if (response['team2']) this.getData('team2');
			if (response['team1-bat']) this.getData('team1-bat');
			if (response['team2-bat']) this.getData('team2-bat');
			if (response['team1-bowl']) this.getData('team1-bowl');
			if (response['team2-bowl']) this.getData('team2-bowl');
		});
	}
	
	ngOnInit(): void {
	}

	getData(key: string) {
		this.fireStore.collection("records").doc(key).ref.get().then((response: any) => {
			this.data = response.data()['html'];
		});
	}
}
