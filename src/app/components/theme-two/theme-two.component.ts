import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { IPL, Match, MatchApp } from '../../classes/app.interface';
import { HelperService } from '../../services/helper.service';

@Component({
  selector: 'app-theme-two',
  templateUrl: './theme-two.component.html',
  styleUrls: ['./theme-two.component.scss']
})
export class ThemeTwoComponent implements OnInit {
	_match!: MatchApp;
	_data =  '';
	_toss = '';
	_special = '';
	hasSpecial = false;
	_isAds = false;

	@Input()
	set match(val: any) {
		this._match = val;
	//	this._match.team1Short = val.team1;
	//	this._match.team2Short = val.team1Short;
	}
	@Input()
	set data(val: any) {
		this._data =  val;
	}
	@Input()
	set toss(val: any) {
		this._toss =  val;
	}
	@Input()
	set special(val: string) {
		if (val && val.length > 0) this.hasSpecial = true;
		else this.hasSpecial = false;

		this._special = val;
	}
	@Input()
	set isAds(val: any) {
		this._isAds =  val;
	}

	constructor(
		private helperService: HelperService,
	) {

	}

	ngOnInit() {
	}

	showArrow(lastStatus: string) {
		let show = false;
		if (lastStatus == 'Ball'
			|| lastStatus == 'Fast Bowler'
			|| lastStatus == 'Stump Out Check'
			|| lastStatus == 'Bowler Stopped'
			|| lastStatus == 'Free Hit'
		) show = true;
		return show;
	}
}
