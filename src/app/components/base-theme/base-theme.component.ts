import { Component, Input, OnInit } from '@angular/core';
import { MatchApp } from '../../classes/app.interface';

@Component({
  selector: 'app-base-theme',
  templateUrl: './base-theme.component.html',
  styleUrls: ['./base-theme.component.scss']
})
export class BaseThemeComponent implements OnInit {
	_match!: MatchApp;
	_data =  '';
	_toss = '';
	_special = '';
	hasSpecial = false;
	isWaiting = false;

	@Input()
	set match(val: any) {
		this._match =  val;
		this.isWaiting = false;
		const lastStatus = this._match.lastStatus;
		if (lastStatus == 'Ball'
			|| lastStatus == 'Stump Out Check'
			|| lastStatus == 'Bowler Stopped'
			|| lastStatus == 'Free Hit'
		) this.isWaiting = true;

		if (lastStatus == '0'
			||lastStatus == '1'
			|| lastStatus == '2'
			|| lastStatus == '3'
			|| lastStatus == '4'
			|| lastStatus == '6'
		) this._match.lastStatus = '';
	}
	@Input()
	set data(val: any) {
		this._data =  val;
	}
	@Input()
	set toss(val: any) {
		this._toss =  val;
	}
	@Input()
	set special(val: string) {
		if (val && val.length > 0) this.hasSpecial = true;
		else this.hasSpecial = false;

		this._special = val;
	}

	constructor() {
	}

	ngOnInit() {
		//console.log(this._match);
	}
}
