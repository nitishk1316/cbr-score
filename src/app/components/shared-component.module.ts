import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ArrowComponent } from './arrow/arrow.component';
import { BaseThemeComponent } from './base-theme/base-theme.component';
import { ThemeOneComponent } from './theme-one/theme-one.component';
import { ThemeThreeComponent } from './theme-three/theme-three.component';
import { ThemeTwoComponent } from './theme-two/theme-two.component';


@NgModule({
  declarations: [
		BaseThemeComponent,
		ThemeOneComponent,
		ThemeTwoComponent,
		ThemeThreeComponent,
		ArrowComponent
	],
  imports: [
		CommonModule
  ],
  exports: [
		BaseThemeComponent,
		ThemeOneComponent,
		ThemeTwoComponent,
		ThemeThreeComponent,
		ArrowComponent
	]
})
export class SharedComponentModule { }
