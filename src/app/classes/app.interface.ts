export interface MatchBatman {
	playerId?: number;
	name?: string;
	r?: number;
	b?: number;
	sr?: string;
	'4s'?: number;
	'6s'?: number;
}

export interface MatchBowler {
	playerId?: number;
	name?: string;
	ov?: number;
	r?: number;
	w?: number;
	d?: number;
	maid?: number;
	e?: number;
	wd?: number;
	nb?: number;
}

export interface MatchOver {
	ovNo: number;
	ovBalls: string[];
}

export interface Match {
	team1?: string;
	team2?: string;
	team1Full?: string;
	team2Full?: string;
	score1?: string;
	score2?: string;
	overs1?: string;
	overs2?: string;
	crr?: string;
	bat1?: MatchBatman;
	bat2?: MatchBatman;
	bowl1?: MatchBowler;
	bowl2?: MatchBowler;
	recentOvs?: MatchOver[] | string;
	status?: string;
	rrr?: number;
	partnerShip?: string;
	teams?: IPLTeam[];
	matchDateMs?: number;
//	showTeam?: boolean;
//	startTime: number;
	lastWkt?: string;
}

export interface IPLPlayer {
	id: number;
	fullName: string;
	shortName: string;
}
export interface IPLTeam {
	id: number;
	team: {
		id: number;
		fullName: string;
		shortName: string;
		abbreviation: string;
	},
	players: IPLPlayer[]
}

export interface IPLInn {
	runRate: string;
	overProgress: string;
	scorecard: {
		runs: number;
		wkts: number;
		battingStats: [{
			playerId: number;
			r: number;
			b: number;
			sr: string;
			'4s': number;
			'6s': number;
		}],
		bowlingStats: [{
			playerId: number;
			ov: number;
			r: number;
			w: number;
			d: number;
			maid: number;
			e: number;
			wd: number;
			nb: number;
		}]
	}
}

export interface IPL {
	matchInfo: {
		matchDateMs: number;
		battingOrder: number[];
		additionalInfo: {
			"toss.elected": string;
			"match.summary": string;
		},
		teams: IPLTeam[];
		inningsSummary: {
			type: string,
			values: number[]
		},
		matchStatus: {
			text: string;
		},
	},
	currentState: {
		currentInningsIndex: number;
		facingBatsman: number;
		nonFacingBatsman: number;
		currentBowler: number;
		previousBowler: number;
		recentOvers: [{
			ovNo: number;
			ovBalls: string[];
		}],
		partnership: {
			runs: number;
			balls: number;
		},
		requiredRunRate: number;
	},
	innings: IPLInn[],

}

export interface MatchTeam {
	team1?: IPLTeam;
	team2?: IPLTeam;
	status?: string;
	team1Full?: string;
	team2Full?: string;
	teams1?: IPLTeam;
	teams2?: IPLTeam;
	startTime?: number;
}


export interface BatmanApp {
	i: string;
	ib: boolean;
	p: string;
	r: number;
	b: number;
	sr: string;
	f: number;
	s: number;
}

export interface BowlerApp {
	i: string;
	p: string;
	o: number;
	r: number;
	w: number;
	m: number;
	e: number;
}

export interface LastBWApp {
	p: string;
	s: string;
}

export interface MatchApp {
	team1Short: string;
	team2Short: string;
	team1: string;
	team2: string;
	score1A?: string;
	score1B?: string;
	score2A?: string;
	score2B?: string;
	over1A?: string;
	over1B?: string;
	over2A?: string;
	over2B?: string;
	crr: string;
	toss: string;
	b1: BatmanApp;
	b2: BatmanApp;
	b3: BowlerApp;
	lb: LastBWApp;
	lw: LastBWApp;
	ro: string[];
	lo?: string[];
	status: string;
	lastStatus: string;
	rrr: string;
	ps: string;
	ovLeft?: string;
}