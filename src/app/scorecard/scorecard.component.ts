import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-scorecard',
  templateUrl: './scorecard.component.html',
  styleUrls: ['./scorecard.component.scss']
})
export class ScorecardComponent implements OnInit {
	data: any;
	inn1: any;
	inn2: any;
	players: any = {};
	team1 = '';
	team2 = '';
	team1Full = '';
	team2Full = '';
  constructor(
		private fireStore: AngularFirestore,
	) {
		this.fireStore.collection("scores").doc("scorecard").valueChanges().subscribe((response: any) => {
			this.data = response;
			this.setPlayers(response.matchInfo.teams[0]);
			this.setPlayers(response.matchInfo.teams[1]);

			const battingOrder = response.matchInfo.battingOrder;
			if (battingOrder.length > 1) {
				this.team1 = response.matchInfo.teams[battingOrder[0]].team.abbreviation;
				this.team2 = response.matchInfo.teams[battingOrder[1]].team.abbreviation;
				this.team1Full = response.matchInfo.teams[battingOrder[0]].team.fullName;
				this.team2Full = response.matchInfo.teams[battingOrder[1]].team.fullName;
			}
			if (response.innings[0]) {
				this.inn1 = response.innings[0];
			}

			if (response.innings[1]){
				this.inn2 = response.innings[1];
			}
		});
	}

  ngOnInit(): void {
	}

	setPlayers(teams: any) {
		const p = teams.players;
		p.forEach((e: any) => {
			this.players[e.id] = e.fullName;
		});
	}

	playerBatData(bat: any, team: string) {
		const image = `//resources.pulse.icc-cricket.com/players/t20wc-2021/284/${bat.playerId}.png`;
		let response = `<div class="d-flex">
			<div class="pl-l">
				<img class="pl-l-i" src="${image}" />
				<div class="pl-ps">
					<div class="pl-ps-t">${team}</div>
				</div>
			</div>
			<div class="pl-r">
				<div class="pl-pr">
					<div class="pl-pr-n mb-4">${this.players[bat.playerId]}</div>
					<div class="pl-pr-c mb-5">${bat.mod ? bat.mod.text : 'NOT OUT' }</div>
					<div class="pl-pr-c d-flex justify-content-between">
						<span>${bat.r} (${bat.b})</span>
						<span>4x ${bat['4s']}</span>
						<span>6x ${bat['6s']}</span>
						<span>SR: ${bat['sr'] || '' }</span>
						<span></span>
						<span></span>
					</div>
				</div>
			</div>
		</div>`;
		this.fireStore.collection("records").doc("facts").update({ players: false });
		this.fireStore.collection("records").doc("players").set({ html: response });
		this.fireStore.collection("records").doc("facts").update({ players: true });
	}

	playerBowlData(bat: any, team: string) {
		const image = `//resources.pulse.icc-cricket.com/players/t20wc-2021/284/${bat.playerId}.png`;
		let response = `<div class="d-flex">
			<div class="pl-l">
				<img class="pl-l-i" src="${image}" />
				<div class="pl-ps">
					<div class="pl-ps-t">${team}</div>
				</div>
			</div>
			<div class="pl-r">
				<div class="pl-pr">
					<div class="pl-pr-n mb-4">${this.players[bat.playerId]}</div>
					<div class="pl-pr-c d-flex justify-content-between">
						<span>${bat.ov} - ${bat.maid} - ${bat.r} - ${bat.w}</span>
						<span>Eco: ${bat.e}</span>
						<span></span>
						<span></span>
					</div>
				</div>
			</div>
		</div>`;
		this.fireStore.collection("records").doc("facts").update({ players: false });
		this.fireStore.collection("records").doc("players").set({ html: response });
		this.fireStore.collection("records").doc("facts").update({ players: true });
	}

	hideAll() {
		this.fireStore.collection("records").doc("content").set({ html: '' });
		//this.fireStore.collection("records").doc("facts").update({ players: false });
	}

	playerShortBat(bat: any, team: string) {
		const image = `//resources.pulse.icc-cricket.com/players/t20wc-2021/284/${bat.playerId}.png`;
		let response = `<div class="player-profile">
			<div class="d-flex pp-p">
				<div class="img-box">
					<img class="" src="${image}" />
				</div>
				<div class="mr-auto pp-b">
					<div class="pp-n">${this.players[bat.playerId]}</div>
					<div class="pp-s">${bat.mod ? bat.mod.text : 'NOT OUT' }</div>
				</div>
			</div>
			<table class="table">
				<tbody>
					<tr>
						<th>RUNS</th>
						<td>${bat.r}</td>
					</tr>
					<tr>
						<th>BALLS</th>
						<td>${bat.b}</td>
					</tr>
					<tr>
						<th>FOURS</th>
						<td>${bat['4s']}</td>
					</tr>
					<tr>
						<th>SIXES</th>
						<td>${bat['6s']}</td>
					</tr>
					<tr>
						<th>STRIKE RATE</th>
						<td>${bat['sr']}</td>
					</tr>
				</tbody>
			</table>
		</div>`;
		this.fireStore.collection("records").doc("content").set({ html: response });
	}

	playerShortBowl(bat: any, team: string) {
		const image = `//resources.pulse.icc-cricket.com/players/t20wc-2021/284/${bat.playerId}.png`;
		let response = `<div class="player-profile">
			<div class="d-flex pp-p">
				<div class="img-box">
					<img class="" src="${image}" />
				</div>
				<div class="mr-auto pp-b">
					<div class="pp-nn">${this.players[bat.playerId]}</div>
				</div>
			</div>
			<table class="table">
				<tbody>
					<tr>
						<th>OVERS</th>
						<td>${bat.ov}</td>
					</tr>
					<tr>
						<th>MAIDENS</th>
						<td>${bat.maid}</td>
					</tr>
					<tr>
						<th>RUNS</th>
						<td>${bat.r}</td>
					</tr>
					<tr>
						<th>WICKETS</th>
						<td>${bat.w}</td>
					</tr>
					<tr>
						<th>ECONOMY</th>
						<td>${bat.e}</td>
					</tr>
				</tbody>
			</table>
		</div>`;
		this.fireStore.collection("records").doc("content").set({ html: response });
	}
}
