import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FactsTodayRoutingModule } from './facts-today-routing.module';
import { FactsTodayComponent } from './facts-today.component';
import { SharedComponentModule } from '../components/shared-component.module';

@NgModule({
  declarations: [FactsTodayComponent],
  imports: [
    CommonModule,
		SharedComponentModule,
	  FactsTodayRoutingModule
  ],
  exports: [FactsTodayComponent]
})
export class FactsTodayExchangeModule { }
