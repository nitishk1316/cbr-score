import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FactsTodayComponent } from './facts-today.component';

const routes: Routes = [
  {
    path: '',
    component: FactsTodayComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FactsTodayRoutingModule { }
