import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FactsTodayComponent } from './facts-today.component';

describe('FactsTodayComponent', () => {
  let component: FactsTodayComponent;
  let fixture: ComponentFixture<FactsTodayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FactsTodayComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FactsTodayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
