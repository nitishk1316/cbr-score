import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'scorecard',
    loadChildren: () => import('./scorecard/scorecard.module').then(m => m.ScorecardModule)
	},
	{
    path: 'cric-edge-exchange',
    loadChildren: () => import('./cric-edge-exchange/cric-edge-exchange.module').then(m => m.CricEdgeExchangeModule)
	},
	{
    path: 'cricket-first-exchange',
    loadChildren: () => import('./cricket-first-exchange/cricket-first.module').then(m => m.CricketFirstExchangeModule)
	},
	{
    path: 'facts-today-exchange',
    loadChildren: () => import('./facts-today-exchange/facts-today.module').then(m => m.FactsTodayExchangeModule)
	},
	{
    path: 'cric-blast-radio',
    loadChildren: () => import('./cric-blast-radio/cric-blast-radio.module').then(m => m.CricBlastRadioModule)
	},
	{
    path: 'teams',
    loadChildren: () => import('./teams/teams.module').then(m => m.TeamsModule)
	},
	{
    path: 'records',
    loadChildren: () => import('./records/preview.module').then(m => m.RecordModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
